import { createGlobalStyle } from 'styled-components';
import WebFont from 'webfontloader';

WebFont.load({
  google: {
    families: [
      'Ubuntu:400,500,700',
      'Roboto:400,500',
      'Open Sans',
      'sans-serif',
    ],
  },
});

export default createGlobalStyle`
  * {
    box-sizing: border-box;
    margin: 0;
    outline: 0;
    padding: 0;
  }
  body {
    background: #FFF;
    color: #22262a;
    font-family: 'Roboto', sans-serif;
    text-rendering: optimizeLegibility !important;
    /* width: 90%; */
  }
  html, body, #root {
    height: 100%;
  }
  input, button {
    font-family: 'Roboto', sans-serif;
  }
  button {
    cursor: pointer;
  }
`;

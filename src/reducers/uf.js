import * as types from '../constants/actionTypes';

const INITIAL_STATE = [];

const applyAddUf = (state, action) => action.funcionarios;

export const ufReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.ADD_UF: {
      return applyAddUf(state, action);
    }
    default:
      return state;
  }
};

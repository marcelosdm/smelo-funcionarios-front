import * as types from '../constants/actionTypes';

const INITIAL_STATE = {
  nome: '',
  error: null,
};

const applyAddNome = (state, action) => ({
  nome: action.funcionarios,
  error: null,
});

const applyFetchErrorNome = (state, action) => ({
  nome: [],
  error: action.error,
});

export const nomeReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.ADD_NOME: {
      return applyAddNome(state, action);
    }
    case types.FETCH_ERROR: {
      return applyFetchErrorNome(state, action);
    }
    default:
      return state;
  }
};

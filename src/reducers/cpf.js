import * as types from '../constants/actionTypes';

const INITIAL_STATE = [];

const applyAddCpf = (state, action) => [...state, action.funcionarios];

export const cpfReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.ADD_CPF: {
      return applyAddCpf(state, action);
    }
    default:
      return state;
  }
};

import * as types from '../constants/actionTypes';

const INITIAL_STATE = [];
const applyAddFuncionarios = (state, action) => action.funcionarios;

export const funcionarioReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.ADD_FUNCIONARIOS: {
      return applyAddFuncionarios(state, action);
    }
    default:
      return state;
  }
};

import * as types from '../constants/actionTypes';

const INITIAL_STATE = {
  cargo: '',
  error: null,
};

const applyAddCargo = (state, action) => ({
  cargo: action.funcionarios,
  error: null,
});

const applyFetchErrorCargo = (state, action) => ({
  cargo: [],
  error: action.error,
});

export const cargoReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.ADD_CARGO: {
      return applyAddCargo(state, action);
    }
    case types.FETCH_ERROR: {
      return applyFetchErrorCargo(state, action);
    }
    default:
      return state;
  }
};

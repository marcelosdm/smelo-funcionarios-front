import { combineReducers } from 'redux';
import { funcionarioReducer } from './funcionario';
import { cpfReducer } from './cpf';
import { cargoReducer } from './cargo';
import { ufReducer } from './uf';
import { nomeReducer } from './nome';

const rootReducer = combineReducers({
  funcionarioState: funcionarioReducer,
  cpfState: cpfReducer,
  cargoState: cargoReducer,
  ufState: ufReducer,
  nomeState: nomeReducer,
});

export default rootReducer;

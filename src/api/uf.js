import { trackPromise } from 'react-promise-tracker';

const BASE_URL = `https://smelo-funcionarios-api.herokuapp.com/v1/funcionarios`;
const URL = `/uf`;

export const fetchByUf = () =>
  trackPromise(
    fetch(BASE_URL + URL, {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    }),
  ).then(response => {
    if (response.ok) {
      return response.json();
    } else if (response.status === 404) {
      return response.userMessage;
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText,
      });
    }
  });

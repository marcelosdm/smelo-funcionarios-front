import { trackPromise } from 'react-promise-tracker';

const query = '';
const BASE_URL = `https://smelo-funcionarios-api.herokuapp.com/v1/funcionarios`;
const CARGO = `/cargo?cargo=${query}`;

export const fetchByCargo = async query =>
  await trackPromise(
    fetch(BASE_URL + CARGO + query, {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    }),
  ).then(response => {
    if (response.ok) {
      return response.json();
    } else if (response.status === 404) {
      return response.userMessage;
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText,
      });
    }
  });

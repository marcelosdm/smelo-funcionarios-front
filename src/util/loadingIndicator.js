import React from 'react';
import styled from 'styled-components';
import Loader from 'react-loader-spinner';
import { usePromiseTracker } from 'react-promise-tracker';

const Div = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  height: 100;
  justify-content: center;
  margin: 2em;
  text-align: center;
  width: '100%';
`;

const Message = styled.p`
  margin: 2em auto;
`;

const LoadingIndicator = props => {
  const { promiseInProgress } = usePromiseTracker();

  return (
    promiseInProgress && (
      <Div>
        <Loader type="TailSpin" color="#1078d8" />
        <Message>Carregando os dados</Message>
      </Div>
    )
  );
};

export default LoadingIndicator;

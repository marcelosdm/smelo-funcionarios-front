export const COLUMNS = {
  nome: {
    label: 'Nome',
    width: '40%',
  },
  cpf: {
    label: 'CPF',
    width: '30%',
  },
  cargo: {
    label: 'Cargo',
    width: '10%',
  },
  salario: {
    label: 'Salário',
    width: '10%',
  },
  uf: {
    label: 'UF Nasc',
    width: '10%',
  },
  dataCad: {
    label: 'Data Cad',
    width: '10%',
  },
  status: {
    label: 'Status',
    width: '10%',
  },
};

export const UF = {
  estado: {
    label: 'Estado',
    width: '10%',
  },
  funcionarios: {
    label: 'Qtde Funcionários',
    width: '10%',
  },
};

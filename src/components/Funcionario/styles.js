import styled from 'styled-components';

export const Table = styled.table`
  background-color: #fff;
  border: 1px solid #999999;
  border-radius: 10px;
  box-shadow: 0 8px 6px -6px black;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 1em;
  padding: 0.5em;
  width: 100%;
`;

export const Dados = styled.div`
  background: #ffffff;
  border: 1px solid #e3e3e3;
  border-radius: 10px;
  display: flex;
  line-height: 24px;
  margin: 10px 0;
  padding: 10px;
  white-space: nowrap;
  :hover {
    background-color: #fff;
    border: 1px solid #555555;
    box-shadow: 0 8px 6px -6px black;
  }
`;

export const Span = styled.span`
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 0 5px;
  :last-child {
    border-radius: 10px;

    background-color: ${props =>
      (props.type === 'INATIVO' && 'lightgrey') ||
      (props.type === 'BLOQUEADO' && 'salmon')};
    border: 2px solid
      ${props =>
        (props.type === 'INATIVO' && 'lightgrey') ||
        (props.type === 'BLOQUEADO' && 'salmon') ||
        (props.type === 'ATIVO' && 'none')};
    color: ${props =>
      (props.type === 'INATIVO' && 'black') ||
      (props.type === 'BLOQUEADO' && 'white')};

    text-align: center;
  }
`;

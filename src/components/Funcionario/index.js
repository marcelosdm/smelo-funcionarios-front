import React from 'react';
import { connect } from 'react-redux';
import { Dados, Span } from './styles';

const Funcionario = ({ funcionario, columns }) => {
  return (
    <Dados>
      <Span style={{ width: columns.nome.width }}>{funcionario.nome}</Span>
      <Span style={{ width: columns.cpf.width }}>{funcionario.cpf}</Span>
      <Span style={{ width: columns.cargo.width }}>{funcionario.cargo}</Span>
      <Span style={{ width: columns.salario.width }}>
        {/* R$ {funcionario.salario.toFixed(2)} */}
        R$ {funcionario.salario}
      </Span>
      <Span style={{ width: columns.uf.width }}>{funcionario.uf}</Span>
      <Span style={{ width: columns.dataCad.width }}>
        {funcionario.dataCad}
      </Span>
      <Span type={funcionario.status} style={{ width: columns.status.width }}>
        {funcionario.status}
      </Span>
    </Dados>
  );
};

export default connect(null)(Funcionario);

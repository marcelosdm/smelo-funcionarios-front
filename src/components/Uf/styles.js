import styled from 'styled-components';

export const Error = styled.p`
  background-color: #f36b56;
  border-radius: 10px;
  box-shadow: 0 4px 6px -6px black;
  font-family: Roboto, sans-serif;
  font-size: 1em;
  font-weight: 500;
  margin: 3em auto;
  padding: 1em;
  text-align: center;
`;

export const Titulo = styled.h2`
  margin: 2em;
`;

export const Spinner = styled.div`
  align-items: center;
  display: flex;
  height: 100;
  justify-content: center;
  text-align: center;
  width: 100%;
`;

export const Message = styled.p`
  margin: 2em;
`;

export const SearchBar = styled.form`
  display: flex;
  justify-content: space-evenly;
  padding: 1em;

  @media (min-width: 1440px) {
    margin: 2em 10em;
  }
`;

export const SearchText = styled.input`
  background-color: #fff;
  border: 1px solid #1078d8;
  border-radius: 12px;
  color: #41464a;
  font-family: Roboto, sans-serif;
  font-size: 1em;
  font-weight: 500;
  margin-right: 1em;
  padding: 0.5em;
  width: 90%;
`;

export const SearchButton = styled.button`
  background-color: #1078d8;
  border: 1px solid #1078d8;
  border-radius: 12px;
  color: #f1f1f1;
  font-family: Roboto, sans-serif;
  font-size: 1em;
  font-weight: 500;
  padding: 0.5em;
`;

export const FuncionariosContainer = styled.ul`
  align-items: center;
  justify-content: center;
  max-width: 15em;
  padding: 0 1em;
  text-align: center;
`;

export const Dados = styled.li`
  background: #ffffff;
  border: 1px solid #e3e3e3;
  border-radius: 10px;
  display: flex;
  line-height: 24px;
  margin: 10px 0;
  padding: 10px;
  text-align: center;
  :hover {
    background-color: #fff;
    border: 1px solid #555555;
    box-shadow: 0 8px 6px -6px black;
  }
`;

export const Span = styled.span`
  display: flex;
  flex-direction: column;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 0 5px;
`;

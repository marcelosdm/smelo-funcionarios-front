import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchByUf } from '../../actions';
import { SearchBar, SearchButton, Titulo } from './styles';

class BuscaPorUf extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    this.props.fetchByUf();
    event.preventDefault();
  }

  render() {
    return (
      <>
        <Titulo>Resultados por UF Nascimento</Titulo>
        <SearchBar onSubmit={this.onSubmit}>
          <SearchButton type="submit">Carregar resultados</SearchButton>
        </SearchBar>
      </>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchByUf: query => dispatch(fetchByUf(query)),
});

export default connect(null, mapDispatchToProps)(BuscaPorUf);

import React from 'react';
import { connect } from 'react-redux';
import LazyLoad from 'react-lazyload';
import Loader from 'react-loader-spinner';
import Details from './details';
import { getFuncionariosByUf } from '../../selectors';
import { FuncionariosContainer, Error, Message, Spinner } from './styles';
import BuscaPorUf from './BuscaPorUf';

const Uf = ({ estados, error }) => {
  return (
    <>
      <BuscaPorUf />
      {error && <Error>Algo deu errado...</Error>}
      {!estados.length ? (
        <></>
      ) : (
        <>
          <Message>Total de Funcionários Obtidos: {estados.length}</Message>
        </>
      )}

      <FuncionariosContainer>
        {(estados || []).map(estado => (
          <LazyLoad
            key={Object.keys}
            height={100}
            offset={[-100, 100]}
            placeholder={
              <Spinner>
                <Loader type="TailSpin" color="#1078d8" />
              </Spinner>
            }
          >
            <Details key={Object.keys} estado={estado} />
          </LazyLoad>
        ))}
      </FuncionariosContainer>
    </>
  );
};

const mapStateToProps = state => ({
  estados: getFuncionariosByUf(state),
});

export default connect(mapStateToProps)(Uf);

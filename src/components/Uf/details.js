import React from 'react';
import { connect } from 'react-redux';
import { Dados, Span } from './styles';

const Details = ({ estado }) => {
  return (
    <Dados>
      <Span>Estado: {estado.uf}</Span>
      <Span>Qtde: {estado.funcionarios}</Span>
    </Dados>
  );
};

export default connect(null)(Details);

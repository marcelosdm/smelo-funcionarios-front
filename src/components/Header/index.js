import React from 'react';
import { Div, Span } from '../Header/styles';

const Header = ({ columns }) => {
  return (
    <Div>
      {Object.keys(columns).map(key => (
        <Span key={key} style={{ width: columns[key].width }}>
          {columns[key].label}
        </Span>
      ))}
    </Div>
  );
};

export default Header;

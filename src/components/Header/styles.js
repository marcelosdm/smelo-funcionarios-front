import styled from 'styled-components';

export const Div = styled.div`
  display: flex;
  line-height: 24px;
  font-size: 16px;
  padding: 0 10px;
  justify-content: space-between;
`;

export const Span = styled.span`
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 0 5px;
`;

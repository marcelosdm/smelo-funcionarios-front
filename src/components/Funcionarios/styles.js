import styled from 'styled-components';

export const FuncionariosContainer = styled.ul`
  align-items: center;
  justify-content: center;
  padding: 0 1em;
`;

export const Error = styled.p`
  background-color: #f36b56;
  border-radius: 10px;
  box-shadow: 0 4px 6px -6px black;
  font-family: Roboto, sans-serif;
  font-size: 1em;
  font-weight: 500;
  margin: 3em auto;
  padding: 1em;
  text-align: center;
`;

export const Spinner = styled.div`
  align-items: center;
  display: flex;
  height: 100;
  justify-content: center;
  text-align: center;
  width: 100%;
`;

export const Message = styled.p`
  margin: 2em;
`;

import React from 'react';
import { connect } from 'react-redux';
import Funcionario from '../Funcionario';
import Header from '../Header';
import { COLUMNS } from '../../util/columns';
import { getFuncionariosByCpf } from '../../selectors/';
import { FuncionariosContainer, Error, Message } from './styles';
import BuscaPorCpf from './BuscaPorCpf';

const Cpf = ({ funcionarios, error }) => {
  return (
    <>
      <BuscaPorCpf />

      {error && <Error>Algo deu errado...</Error>}
      {!funcionarios.length ? (
        <></>
      ) : (
        <>
          <Message>
            Total de Funcionários Obtidos: {funcionarios.length}
          </Message>
          <Header columns={COLUMNS} />
        </>
      )}

      <FuncionariosContainer>
        {(funcionarios || []).map(funcionario => (
          <Funcionario
            key={funcionario.cpf}
            funcionario={funcionario}
            columns={COLUMNS}
          />
        ))}
      </FuncionariosContainer>
    </>
  );
};

const mapStateToProps = state => ({
  funcionarios: getFuncionariosByCpf(state),
});

export default connect(mapStateToProps)(Cpf);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchByNome } from '../../actions';
import { SearchBar, SearchText, SearchButton, Titulo } from './styles';

class BuscaPorNome extends Component {
  constructor(props) {
    super(props);

    this.state = {
      query: '',
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    const { query } = this.state;
    if (query) {
      this.props.fetchByNome(query);

      this.setState({ query: '' });
    }
    event.preventDefault();
  }

  onChange(event) {
    const { value } = event.target;

    this.setState({ query: value });
  }

  render() {
    return (
      <>
        <Titulo>Busca por Nome</Titulo>
        <SearchBar onSubmit={this.onSubmit}>
          <SearchText
            type="search"
            value={this.state.query}
            onChange={this.onChange}
          />
          <SearchButton type="submit">Buscar</SearchButton>
        </SearchBar>
      </>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchByNome: query => dispatch(fetchByNome(query)),
});

export default connect(null, mapDispatchToProps)(BuscaPorNome);

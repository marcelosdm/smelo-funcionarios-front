import styled from 'styled-components';

export const FuncionariosContainer = styled.ul`
  align-items: center;
  justify-content: center;
  padding: 0 1em;
`;

export const Titulo = styled.h2`
  margin: 2em;
`;

export const Error = styled.p`
  background-color: #f36b56;
  border-radius: 10px;
  box-shadow: 0 4px 6px -6px black;
  font-family: Roboto, sans-serif;
  font-size: 1em;
  font-weight: 500;
  margin: 3em auto;
  padding: 1em;
  text-align: center;
`;

export const Spinner = styled.div`
  align-items: center;
  display: flex;
  height: 100;
  justify-content: center;
  text-align: center;
  width: 100%;
`;

export const Message = styled.p`
  margin: 2em;
`;

export const SearchBar = styled.form`
  display: flex;
  justify-content: space-evenly;
  padding: 1em;
`;

export const SearchText = styled.input`
  background-color: #fff;
  border: 1px solid #1078d8;
  border-radius: 12px;
  color: #41464a;
  font-family: Roboto, sans-serif;
  font-size: 1em;
  font-weight: 500;
  margin-right: 1em;
  padding: 0.5em;
  width: 90%;
`;

export const SearchButton = styled.button`
  background-color: #1078d8;
  border: 1px solid #1078d8;
  border-radius: 12px;
  color: #f1f1f1;
  font-family: Roboto, sans-serif;
  font-size: 1em;
  font-weight: 500;
  padding: 0.5em;
`;

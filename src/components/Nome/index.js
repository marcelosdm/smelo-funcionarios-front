import React from 'react';
import { connect } from 'react-redux';
import LazyLoad from 'react-lazyload';
import Loader from 'react-loader-spinner';
import Funcionario from '../Funcionario';
import Header from '../Header';
import { COLUMNS } from '../../util/columns';
import { getFuncionariosByNome, fetchErrorNome } from '../../selectors';
import { FuncionariosContainer, Error, Message, Spinner } from './styles';
import BuscaPorNome from './BuscaPorNome';

const Nome = ({ funcionarios, error }) => {
  return (
    <>
      <BuscaPorNome />

      {error && <Error>Algo deu errado...</Error>}
      {!funcionarios ? (
        <></>
      ) : (
        <>
          <Message>
            Total de Funcionários Obtidos: {funcionarios.length}
          </Message>
          <Header columns={COLUMNS} />
        </>
      )}

      <FuncionariosContainer>
        {(funcionarios || []).map(funcionario => (
          <LazyLoad
            key={funcionario.cpf}
            height={100}
            offset={[-100, 100]}
            placeholder={
              <Spinner>
                <Loader type="TailSpin" color="#1078d8" />
              </Spinner>
            }
          >
            <Funcionario
              key={funcionario.cpf}
              funcionario={funcionario}
              columns={COLUMNS}
            />
          </LazyLoad>
        ))}
      </FuncionariosContainer>
    </>
  );
};

const mapStateToProps = state => ({
  funcionarios: getFuncionariosByNome(state),
  error: fetchErrorNome(state),
});

export default connect(mapStateToProps)(Nome);

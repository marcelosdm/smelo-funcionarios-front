import React from 'react';
import { Link } from 'react-router-dom';
import { Nav, Ul, Li } from './styles';

const Navbar = () => (
  <Nav>
    <Ul>
      <Li>
        <Link to="/">Home</Link>
      </Li>
      <Li>
        <Link to="/cpf">CPF</Link>
      </Li>
      <Li>
        <Link to="/cargo">Cargo</Link>
      </Li>
      <Li>
        <Link to="/uf_nasc">UF Nascimento</Link>
      </Li>
      <Li>
        <Link to="/todos">Todos</Link>
      </Li>
    </Ul>
  </Nav>
);

export default Navbar;

import styled from 'styled-components';

export const Nav = styled.nav`
  align-items: center;
  background: #1078d8;
  display: flex;
  justify-content: space-around;
`;

export const Ul = styled.ul`
  display: flex;
  justify-content: space-around;
  list-style-type: none;
`;

export const Li = styled.li`
  padding: 0.5em;
  a {
    color: #fff;
    text-decoration: none;
    font-weight: 500;
  }
  @media (min-width: 768px) {
    padding: 2em;
  }
`;

export const Logo = styled.img`
  content: url('labs-logo.svg');
`;

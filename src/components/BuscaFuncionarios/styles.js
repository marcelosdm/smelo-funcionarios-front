import styled from 'styled-components';

export const SearchBar = styled.form`
  display: flex;
  justify-content: space-evenly;
  padding: 1em;

  @media (min-width: 1440px) {
    margin: 2em 10em;
  }
`;

export const Titulo = styled.h2`
  margin: 2em;
`;

export const SearchText = styled.input`
  background-color: #fff;
  border: 1px solid #1078d8;
  border-radius: 12px;
  color: #41464a;
  font-family: Roboto, sans-serif;
  font-size: 1em;
  font-weight: 500;
  margin-right: 1em;
  padding: 0.5em;
  width: 90%;
`;

export const SearchButton = styled.button`
  background-color: #1078d8;
  border: 1px solid #1078d8;
  border-radius: 12px;
  color: #f1f1f1;
  font-family: Roboto, sans-serif;
  font-size: 1em;
  font-weight: 500;
  padding: 0.5em;
`;

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchFuncionarios } from '../../actions/';
import { SearchBar, SearchText, SearchButton, Titulo } from './styles';

class BuscaFuncionarios extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    this.props.fetchFuncionarios();
    event.preventDefault();
  }

  render() {
    return (
      <>
        <Titulo>Busca por Nome</Titulo>
        <SearchBar onSubmit={this.onSubmit}>
          <SearchButton type="submit">
            Buscar todos os Funcionários
          </SearchButton>
        </SearchBar>
      </>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchFuncionarios: query => dispatch(fetchFuncionarios(query)),
});

export default connect(null, mapDispatchToProps)(BuscaFuncionarios);

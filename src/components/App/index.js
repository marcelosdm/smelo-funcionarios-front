import React from 'react';
import Funcionarios from '../Funcionarios';
import CPF from '../CPF';
import Cargo from '../Cargo';
import Navbar from '../Navbar';
import Nome from '../Nome';
import Uf from '../Uf';
import LoadingIndicator from '../../util/loadingIndicator';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import GlobalStyle from '../../styles/globalStyle';
import { Header, Title, Titles, Content, Subtitle } from './styles';

const App = () => {
  return (
    <Router>
      <GlobalStyle />

      <Header>
        <Titles>
          <Title>Funcionários</Title>
          <Subtitle>Desafio Técnico Luizalabs</Subtitle>
        </Titles>
        <Navbar />
      </Header>

      <Content>
        <LoadingIndicator />
        <Switch>
          <Route exact path="/" component={Nome} />
          <Route exact path="/todos" component={Funcionarios} />
          <Route exact path="/cpf" component={CPF} />
          <Route exact path="/cargo" component={Cargo} />
          <Route exact path="/uf_nasc" component={Uf} />
        </Switch>
      </Content>
    </Router>
  );
};

export default App;

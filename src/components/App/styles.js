import styled from 'styled-components';

export const Header = styled.header`
  align-items: center;
  box-shadow: 0 8px 6px -6px black;
  background-color: #1078d8;
  color: #f2f2f2;
  display: flex;
  justify-content: space-around;
  margin-bottom: 1em;
  min-height: 80px;
  padding: 0.25em;
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 99;
`;

export const Titles = styled.div`
  padding: 0 1em;
`;

export const Title = styled.h1`
  font-family: 'Ubuntu', sans-serif;
  font-size: calc(30px + (26 - 14) * ((100vw - 300px) / (1600 - 300)));
`;

export const Subtitle = styled.h2`
  font-family: 'Roboto', sans-serif;
  font-size: calc(15px + (26 - 14) * ((100vw - 300px) / (1600 - 300)));
  font-weight: 400;
`;

export const Content = styled.main`
  position: relative;
  top: 100px;
`;

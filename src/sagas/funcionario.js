import { call, put } from 'redux-saga/effects';
import { addFuncionarios } from '../actions/';
import { fetchFuncionarios } from '../api/funcionario';

function* handleFetchFuncionarios() {
  const result = yield call(fetchFuncionarios);
  yield put(addFuncionarios(result));
}

export { handleFetchFuncionarios };

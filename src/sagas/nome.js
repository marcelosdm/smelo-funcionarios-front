import { call, put } from 'redux-saga/effects';
import { addNome, fetchError } from '../actions';
import { fetchByNome } from '../api/nome';

function* handleFetchByNome(action) {
  const { query } = action;
  try {
    const result = yield call(fetchByNome, query);
    yield put(addNome(result));
  } catch (e) {
    yield put(fetchError(e));
  }
}

export { handleFetchByNome };

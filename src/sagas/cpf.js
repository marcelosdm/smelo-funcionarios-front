import { call, put } from 'redux-saga/effects';
import { addCpf } from '../actions/';
import { fetchByCpf } from '../api/cpf';

function* handleFetchByCpf(action) {
  const { query } = action;

  try {
    const result = yield call(fetchByCpf, query);
    yield put(addCpf(result));
  } catch (e) {
    console.log(e);
  }
}

export { handleFetchByCpf };

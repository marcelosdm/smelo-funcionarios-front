import { call, put } from 'redux-saga/effects';
import { addCargo, fetchError } from '../actions';
import { fetchByCargo } from '../api/cargo';

function* handleFetchByCargo(action) {
  const { query } = action;

  try {
    const result = yield call(fetchByCargo, query);
    yield put(addCargo(result));
  } catch (e) {
    yield put(fetchError(e));
  }
}

export { handleFetchByCargo };

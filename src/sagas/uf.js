import { call, put } from 'redux-saga/effects';
import { addUf } from '../actions';
import { fetchByUf } from '../api/uf';

function* handleFetchByUf(action) {
  const result = yield call(fetchByUf);
  yield put(addUf(result));
}

export { handleFetchByUf };

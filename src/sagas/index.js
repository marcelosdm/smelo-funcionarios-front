import * as types from '../constants/actionTypes';
import { takeLatest, all } from 'redux-saga/effects';
import { handleFetchFuncionarios } from './funcionario';
import { handleFetchByCpf } from './cpf';
import { handleFetchByCargo } from './cargo';
import { handleFetchByUf } from './uf';
import { handleFetchByNome } from './nome';

function* watchAll() {
  yield all([
    takeLatest(types.FETCH_FUNCIONARIOS, handleFetchFuncionarios),
    takeLatest(types.FETCH_BY_CPF, handleFetchByCpf),
    takeLatest(types.FETCH_BY_CARGO, handleFetchByCargo),
    takeLatest(types.FETCH_BY_CARGO, handleFetchByCargo),
    takeLatest(types.FETCH_BY_UF, handleFetchByUf),
    takeLatest(types.FETCH_BY_NOME, handleFetchByNome),
  ]);
}

export default watchAll;

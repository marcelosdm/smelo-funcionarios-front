export const getFuncionarios = ({ funcionarioState }) => funcionarioState;

export const getFuncionariosByNome = ({ nomeState }) => nomeState.nome;
export const fetchErrorNome = ({ nomeState }) => nomeState.error;

export const getFuncionariosByCpf = ({ cpfState }) => cpfState;

export const getFuncionariosByCargo = ({ cargoState }) => cargoState.cargo;
export const fetchErrorCargo = ({ cargoState }) => cargoState.error;

export const getFuncionariosByUf = ({ ufState }) => ufState;

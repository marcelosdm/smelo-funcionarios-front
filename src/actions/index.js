import * as types from '../constants/actionTypes';

export const addFuncionarios = funcionarios => ({
  type: types.ADD_FUNCIONARIOS,
  funcionarios,
});

export const fetchFuncionarios = () => ({
  type: types.FETCH_FUNCIONARIOS,
});

export const addNome = funcionarios => ({
  type: types.ADD_NOME,
  funcionarios,
});

export const fetchByNome = query => ({
  type: types.FETCH_BY_NOME,
  query,
});

export const addCpf = funcionarios => ({
  type: types.ADD_CPF,
  funcionarios,
});

export const fetchByCpf = query => ({
  type: types.FETCH_BY_CPF,
  query,
});

export const addCargo = funcionarios => ({
  type: types.ADD_CARGO,
  funcionarios,
});

export const fetchByCargo = query => ({
  type: types.FETCH_BY_CARGO,
  query,
});

export const addUf = funcionarios => ({
  type: types.ADD_UF,
  funcionarios,
});

export const fetchByUf = () => ({
  type: types.FETCH_BY_UF,
});

export const fetchError = () => ({
  type: types.FETCH_ERROR,
});

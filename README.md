# FrontEnd para API de Funcionários

Projeto de FrontEnd do desafio técnico para o **Luiza Labs**.

Este projeto foi desenvolvido utilizando o **create-react-app**.

## Começando

Para de rodar o projeto em máquina local, siga os passos:

1. Baixe ou clone este repositório
2. Acesse o diretório do projeto: `cd funcionarios-frontend`
3. Execute o comando `yarn install` para instalar todas as dependências do projeto.
4. Acesse o projeto em um editor de texto ou IDE.

### Iniciando

Após a instalação das dependências, a aplicação está pronta para ser inicializada.

Para inicializar, no terminal, basta executar o comando `yarn start`. Após isto, a aplicação estará disponível no endereço local `http://localhost:3000/`.

## Como Funciona

Para utilizar a aplicação, no menu de navegação estão disponíveis as buscas/filtros. Por padrão, a busca _default_ no início da aplicação é a **Busca por Nome**. O usuário pode selecionar a busca que deseja, que a aplicação estará pronta para receber a query.

Caso necessário, um exemplo desta aplicação rodando está disponível neste endereço: ]https://smelo-funcionarios-frontend.herokuapp.com/ .

A aplicação de exemplo citada acima, já está buscando os dados da API hospedada também ne _Heroku_, neste endereço: https://smelo-funcionarios-api.herokuapp.com/v1/funcionarios.

Caso necessário, a última opção do menu, chamada **Todos**, busca todos os usuários da base de dados.

### Melhorias

Pontos a serem implementados/aprimorados:

Algumas funcionalidades não foram possíveis de serem desenvolvidas por questão de tempo. São elas:

- Implementar testes;
- Unificar todos os componentes de busca em apenas um;
- Formulário para castro de novos funcionários;
- Tela de login de usuário;
- Tela para criação de usuário;

## Ferramentas e Bibliotecas

- [React](https://reactjs.org/) - Biblioteca Javascript
- [create-react-app](https://create-react-app.dev/) - Ferramenta para criação de projetos React
- [Yarn](https://yarnpkg.com/) - Ferramenta para gerenciamento de dependências
- [Styled Components](https://www.styled-components.com/) - Biblioteca de CSS

## Contribuições

Embora este seja um desafio técnico, que será acessado somente pelos envolvidos no processo de seleção, estou aberto à feedbacks e ficaria muito feliz em receber sugestões e críticas.

## Desenvolvedor

- [**Marcelo Melo**](https://github.com/marcelosdm)
